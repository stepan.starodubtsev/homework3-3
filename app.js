while (true){
    let userAge = prompt('Enter your age:');
    if (userAge !== "") {
        if (userAge < 12) {
            alert('You`re child');
        } else if (userAge >= 12 && userAge < 18) {
            alert('You`re teenager');
            break;
        } else {
            alert('Wrong age. Enter your age please');
        }
    } else {
        alert('Enter your age please');
    }
}


let monthName = prompt('Enter month in ukrainian:').toLowerCase();

let days;

switch (monthName) {
    case 'січень':
    case 'березень':
    case 'травень':
    case 'липень':
    case 'серпень':
    case 'жовтень':
    case 'грудень':
        days = 31;
        break;
    case 'квітень':
    case 'червень':
    case 'вересень':
    case 'листопад':
        days = 30;
        break;
    case 'лютий':
        days = 28;
        break;
    default:
        days = null;
}

if (days) {
    alert(`Місяць ${monthName} має ${days} днів`);
} else {
    alert("Невірно введений місяць. Будь ласка, введіть місяць українською мовою");
}
